<?php

namespace App\Http\Responses\Backend\Access\User;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var \App\Models\Access\User\User
     */
    protected $user;
    
    /**
     * @var \App\Models\Access\Role\Role
     */
    protected $roles;

    /**
     * @param \App\Models\Access\User\User $user
     */
    public function __construct($user, $roles)
    {
        $this->user = $user;
        $this->roles = $roles;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.access.users.edit')->with([
            'user'            => $this->user,
            'userRoles'       => $this->user->roles->pluck('id')->all(),
            'roles'           => $this->roles,
        ]);
    }
}
