<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Repositories\Frontend\Access\User\UserRepository;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\Backend\Access\Role\RoleRepository;

/**
 * Class RegisterController.
 */
class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * RegisterController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user,RoleRepository $roles)
    {
        // Where to redirect users after registering
        $this->redirectTo = route('frontend.index');

        $this->user = $user;
        $this->roles = $roles;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $roles = $this->roles->getAll();
        $symptoms = \DB::table('symptoms')->pluck('symptom','id')->toArray();
        return view('frontend.auth.register',compact('roles','symptoms'));
    }

    /**
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request)
    {
        
        $user = $this->user->create($request->all());
    
        if($request->role[0] == '3')
        {
            \Auth::logout();
            return redirect()->route('frontend.auth.login')->withFlashSuccess(trans('Your account was successfully created.'));
        } else {
            \Auth::logout();
            return redirect()->route('frontend.auth.login')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.created_pending'));
        }
    }
}
