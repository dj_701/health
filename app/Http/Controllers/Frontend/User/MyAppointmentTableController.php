<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\User\UserRepository;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;

/**
 * Class UserTableController.
 */
class MyAppointmentTableController extends Controller
{
    /**
     * @var \App\Repositories\Backend\Access\User\UserRepository
     */
    protected $users;

    /**
     * @param \App\Repositories\Backend\Access\User\UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * @param \App\Http\Requests\Backend\Access\User\ManageUserRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageUserRequest $request)
    {
        return Datatables::make($this->users->getForMyAppointmentsDataTable($request->get('status'), $request->get('trashed')))
            ->escapeColumns(['first_name', 'appointment_date', 'appointment_date'])
            ->addColumn('is_appointment_approved', function ($user) {
                    if(!empty($user->is_appointment_approved))
                    {
                        return 'Approved';
                    } else {
                        return 'Pending';
                    }
            })
            ->make(true);
    }
}
