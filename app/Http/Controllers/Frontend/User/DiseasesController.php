<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\User\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class UserTableController.
 */
class DiseasesController extends Controller
{
    /**
     * @var \App\Repositories\Backend\Access\User\UserRepository
     */
    protected $users;

    /**
     * @param \App\Repositories\Backend\Access\User\UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * @param \App\Http\Requests\Backend\Access\User\ManageUserRequest $request
     *
     * @return mixed
     */
    public function index()
    {
        return view('frontend.user.myappointments');
    }

    /**
     * @param \App\Http\Requests\Backend\Access\User\ManageUserRequest $request
     *
     * @return mixed
     */
    public function bookAppointment(Request $request)
    {
        $array = array(
                'patient_id' => access()->user()->id,
                'doctor_id' => $request['doctor_id'],
                'disease' => implode(',',$request['disease']),
                'appointment_date' => $request['appointment_date'],
                'appointment_time' => $request['appointment_time']
        );
        $insert = \DB::table('appointments')->insert($array);
        return redirect()->back()->withFlashSuccess('Appointment Booked.');
    }
}
