<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\DashboardViewRequest;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(DashboardViewRequest $request)
    {
    	if(access()->user()->hasRole('Doctor'))
    	{ 
			return view('frontend.user.dashboard');
    	} else {
    		$allPredictedDisease = \DB::table('predict_diseases')->whereIn('symptom_id',explode(',',access()->user()->symptoms))->get();
    		return view('frontend.user.diseases',compact('allPredictedDisease'));
    	}
        return view('frontend.user.dashboard');
    }
}
