<?php

namespace App\Http\Requests\Frontend\Auth;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class RegisterRequest.
 */
class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->role[0] == '3')
        {
            return [
                        'first_name'           => 'required|max:255',
                        'last_name'            => 'required|max:255',
                        'email'                => ['required', 'email', 'max:255', Rule::unique('users')],
                        'address'                => 'required',
                        'symptoms'                => 'required',
                        'profession'                => 'required',
                        'location'                => 'required',
                    ];
        } else {
            return [
                'first_name'           => 'required|max:255',
                'last_name'            => 'required|max:255',
                'email'                => ['required', 'email', 'max:255', Rule::unique('users')],
                'address'                => 'required',
                'doctor_skill'                => 'required',
                'location'                => 'required',
            ];
        }
        
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
