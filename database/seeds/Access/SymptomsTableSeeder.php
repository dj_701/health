<?php

use Carbon\Carbon as Carbon;
use Database\DisableForeignKeys;
use Database\TruncateTable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class UserTableSeeder.
 */
class SymptomsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate('symptoms');

        //Add the master administrator, user id of 1
        $users = [
            [
                'symptom'        => 'Headache',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'symptom'        => 'Cough',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]
        ];

        DB::table('symptoms')->insert($users);

        $this->enableForeignKeys();

        $this->disableForeignKeys();
        $this->truncate('predict_diseases');

        //Add the master administrator, user id of 1
        $diseases = [
            [
                'symptom_id'        => '1',
                'disease'           => 'Common Cold',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'symptom_id'        => '2',
                'disease'           => 'Asthama',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]
        ];

        DB::table('predict_diseases')->insert($diseases);

        $this->enableForeignKeys();
    }
}
