<?php

use Carbon\Carbon as Carbon;
use Database\DisableForeignKeys;
use Database\TruncateTable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate(config('access.users_table'));

        //Add the master administrator, user id of 1
        $users = [
            [
                'first_name'        => 'Deep',
                'last_name'         => 'Joshi',
                'email'             => 'admin@admin.com',
                'password'          => bcrypt('1234'),
                'address'             => 'Address 1',
                'symptoms'             => Null,
                'patient_profession'             => Null,
                'doctor_skill'             => Null,
                'location'             => Null,
                'created_by'        => 1,
                'updated_by'        => null,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
                'deleted_at'        => null,
            ],
            [
                'first_name'        => 'Doctor',
                'last_name'         => 'Test',
                'email'             => 'doctor@test.com',
                'password'          => bcrypt('1234'),
                'address'             => 'Address 1',
                'symptoms'             => Null,
                'patient_profession'             => Null,
                'doctor_skill'             => 'Asthama',
                'location'             => 'Pune',
                'created_by'        => 1,
                'updated_by'        => null,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
                'deleted_at'        => null,
            ],
            [
                'first_name'        => 'Patient',
                'last_name'         => 'Test',
                'email'             => 'Patient@test.com',
                'password'          => bcrypt('1234'),
                'address'             => 'Address 1',
                'symptoms'             => '1',
                'patient_profession'             => 'IT',
                'doctor_skill'             => null,
                'location'             => 'Nanded',
                'created_by'        => 1,
                'updated_by'        => null,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
                'deleted_at'        => null,
            ],
        ];

        DB::table(config('access.users_table'))->insert($users);

        $this->enableForeignKeys();
    }
}
