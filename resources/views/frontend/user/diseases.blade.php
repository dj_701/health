@extends('frontend.layouts.app')

@section('content')
<div class="box-header with-border">
            <h3 class="box-title">{{ trans('Doctors') }}</h3>

            <div class="box-tools pull-right">
                <div class="btn-group">
          <button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">Export
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li id="copyButton"><a href="#"><i class="fa fa-clone"></i>Copy</a></li>
            <li id="csvButton"><a href="#"><i class="fa fa-file-text-o"></i> CSV</a></li>
            <li id="excelButton"><a href="#"><i class="fa fa-file-excel-o"></i> Excel</a></li>
            <li id="pdfButton"><a href="#"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
            <li id="printButton"><a href="#"><i class="fa fa-print"></i> Print</a></li>
          </ul>
        </div>
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->
   <div class="box box-info">
        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="diseases-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.access.users.table.first_name') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.last_name') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.email') }}</th>
                            <th>{{ trans('Location') }}</th>
                            <th>{{ trans('Skill') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.created') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.last_updated') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th>
                                </th>
                            <th>
                                
                            </th>
                            <th>
                                
                            </th>
                            <th>
                             {!! Form::text('location', null, ["class" => "search-input-text form-control", "data-column" => 3, "placeholder" => trans('location')]) !!}
                                    <a class="reset-data" href="javascript:void(0)"><i class="fa fa-times"></i></a>
                            </th>
                            <th>
                            
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <!--<div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {{-- {!! history()->renderType('User') !!} --}}
        </div><!-- /.box-body -->
    </div><!--box box-info-->

    <!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="js-title-step"></h4>

      </div>

      <div class="modal-body">
        <div class="alert alert-danger error-class" style="display: none;"></div>
        {{ Form::open(['route' => 'frontend.user.bookAppointment','id'=>'bookAppointment' ,'class' => 'form-horizontal']) }}
            <span>Predicted Diseases</span>
            @foreach($allPredictedDisease as $disease)
                <!-- Material unchecked disabled -->
                <div class="form-check">
                    <input type="hidden" name="disease[]" value="{{$disease->disease}}">
                    -- {{$disease->disease}}
                <!-- <input type="checkbox" name="disease[]" class="form-check-input selected-disease" value="{{$disease->id}}" id="materialUncheckedDisabled{{$disease->id}}">
                <label class="form-check-label" for="materialUncheckedDisabled{{$disease->id}}">{{$disease->disease}}</label>
                --></div>
                <!-- $disease->id -->
            @endforeach
            <input type="hidden" name="doctor_id" id="doctor_id">
            <!-- allPredictedDisease -->
                <div><h6>Doctor Skill :</h6> <span class="skill"></span></div><br>
                <div><h6>Doctor Location :</h6> <span class="location"></span></div>
          <div class="jumbotron">
                        <div>
                        {{ Form::label('appointment_date', trans('Appointment Date').'*', ['class' => 'col-md-4 control-label']) }}
                            {{ Form::input('appointment_date', 'appointment_date', null, ['class' => 'form-control datetimepicker1','placeholder'=>'Select Date']) }}
                </div>
                <div>
                        {{ Form::label('appointment_time', trans('Appointment Time').'*', ['class' => 'col-md-4 control-label']) }}
                            {{ Form::input('appointment_time', 'appointment_time', null, ['class' => 'form-control datetimepicker2','placeholder'=>'Select time']) }}
          </div></div>
        {{ Form::close() }}
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default js-btn-step pull-left" data-orientation="cancel" data-dismiss="modal">Cancel</button>

        <button type="button" class="btn btn-success js-btn-step bookAppointment" data-orientation="next">Book Appointment</button>

      </div>

    </div>

  </div>
</div>
@endsection


@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        (function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            
            var dataTable = $('#diseases-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("frontend.user.getDiseases") }}',
                    type: 'post',
                    data: {status: 1, trashed: false}
                },
                columns: [

                    {data: 'first_name', name: '{{config('access.users_table')}}.first_name'},
                    {data: 'last_name', name: '{{config('access.users_table')}}.last_name'},
                    {data: 'email', name: '{{config('access.users_table')}}.email'},
                    {data: 'location', name: '{{config('access.users_table')}}.location', sortable: false},
                    {data: 'doctor_skill', name: '{{config('access.users_table')}}.doctor_skill', sortable: false},
                    {data: 'created_at', name: '{{config('access.users_table')}}.created_at'},
                    {data: 'updated_at', name: '{{config('access.users_table')}}.updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        })();

        $(document).ready(function(){
            $(document).on('click','.book-appointment',function(){
                $('#myModal').modal('show');
                $('.location').text($(this).data('location'));
                $('.skill').text($(this).data('value'));
                $('#doctor_id').val($(this).data('id'));

            });

            var nowDate = new Date();
    var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
    $('.datetimepicker1').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: today,
    });
            // $('.datetimepicker1').datetimepicker({
            //         format : 'Y-m-d'
            // });
            $('.datetimepicker2').datetimepicker({
                    format : 'LT'
            });

            $(document).on('click','.bookAppointment',function(){
                validate();
            });

            function validate(){
                // var atLeastOneIsChecked = $('input[name="disease[]"]:checked').length > 0;

                if($('#doctor_id').val() == '')
                {
                    $('.error-class').html('Please Doctor Id');
                    $('.error-class').show();
                    return false;
                }

                if($('.datetimepicker1').val() == '' || $('.datetimepicker2').val() == '')
                {
                    $('.error-class').html('Please select date and time for appointment.');
                    $('.error-class').show();
                    return false;
                }

                $('.error-class').hide();
                $('#bookAppointment').submit();
            }

        });
    </script>
@endsection