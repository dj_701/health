{{ Form::model($logged_in_user, ['route' => 'frontend.user.profile.update', 'class' => 'form-horizontal', 'method' => 'PATCH']) }}

    <div class="form-group">
        {{ Form::label('first_name', trans('validation.attributes.frontend.register-user.firstName'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::input('text', 'first_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.firstName')]) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('last_name', trans('validation.attributes.frontend.register-user.lastName'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::input('text', 'last_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.firstName')]) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('address', trans('validation.attributes.frontend.register-user.address'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::input('textarea', 'address', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.address')]) }}
        </div>
    </div>


    {{-- city --}}
    <div class="form-group">
        {{ Form::label('city_id', trans('City'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::input('textarea', 'city', null, ['class' => 'form-control', 'placeholder' => trans('City')]) }}
        </div><!--col-md-6-->
    </div><!--form-group-->


    {{-- state --}}
    <div class="form-group">
        {{ Form::label('state_id', trans('State'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
        {{ Form::input('textarea', 'state', null, ['class' => 'form-control', 'placeholder' => trans('State')]) }}
        </div><!--col-md-6-->
    </div><!--form-group-->

    <div class="form-group">
        {{ Form::label('Country', trans('Country'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
        {{ Form::input('textarea', 'country', null, ['class' => 'form-control', 'placeholder' => trans('Country')]) }}
        </div><!--col-md-6-->
    </div><!--form-group-->

    {{-- zipcode --}}
    <div class="form-group">
        {{ Form::label('zip_code', trans('validation.attributes.frontend.register-user.zipcode'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::input('name', 'zip_code', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.zipcode')]) }}
        </div><!--col-md-6-->
    </div><!--form-group-->
                    
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            {{ Form::submit(trans('labels.general.buttons.update'), ['class' => 'btn btn-primary', 'id' => 'update-profile']) }}
        </div>
    </div>

{{ Form::close() }}