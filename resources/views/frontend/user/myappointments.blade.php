@extends('frontend.layouts.app')

@section('content')
   <div class="box box-info">
        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="diseases-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('Doctor Name') }}</th>
                            <th>{{ trans('Date') }}</th>
                            <th>{{ trans('Time') }}</th>
                            <th>{{ trans('Status') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th>
                                </th>
                            <th>
                                
                            </th>
                            <th>
                                
                            </th>
                            <th>
                            
                            </th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <!--<div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {{-- {!! history()->renderType('User') !!} --}}
        </div><!-- /.box-body -->
    </div><!--box box-info-->

    <!-- Modal -->
@endsection


@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        (function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            
            var dataTable = $('#diseases-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("frontend.user.myappointments") }}',
                    type: 'post',
                    data: {status: 1, trashed: false}
                },
                columns: [

                    {data: 'first_name', name: 'doctor.first_name'},
                    {data: 'appointment_date', name: 'appointments.appointment_date'},
                    {data: 'appointment_time', name: 'appointments.appointment_time'},
                    {data: 'is_appointment_approved', name: 'appointments.is_appointment_approved'},
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
            });
        })();
    </script>
@endsection