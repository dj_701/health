@extends('frontend.layouts.app')

@section('content')
    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('labels.frontend.auth.register_box_title') }}</div>

                <div class="panel-body">

                    {{ Form::open(['route' => 'frontend.auth.register', 'class' => 'form-horizontal']) }}
                    
                    <div class="form-group">
                        {{ Form::label('first_name', trans('validation.attributes.frontend.register-user.firstName').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('name', 'first_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.firstName')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('last_name', trans('validation.attributes.frontend.register-user.lastName').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('name', 'last_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.lastName')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('email', trans('validation.attributes.frontend.register-user.email').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.email')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('password', trans('validation.attributes.frontend.register-user.password').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('password_confirmation', trans('validation.attributes.frontend.register-user.password_confirmation').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password_confirmation')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->
<!-- style="display: none" -->

                    <div class="form-group">
                        {{ Form::label('address', trans('Address').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('address', 'address', null, ['class' => 'form-control', 'placeholder' => trans('Address')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="Patient-div">
                    <div class="form-group">
                        {{ Form::label('symptoms', trans('Symptoms').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::select('symptoms[]',$symptoms,null,array('multiple')) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('profession', trans('Profession').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('profession', 'profession', null, ['class' => 'form-control', 'placeholder' => trans('Profession')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->
                    </div>

                    <div class="Doctor-div" style="display: none">
                    <div class="form-group">
                        {{ Form::label('doctor_skill', trans('Specialised skills').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('doctor_skill', 'doctor_skill', null, ['disabled','class' => 'form-control', 'placeholder' => trans('Specialised skills')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->
                    </div>

                    <div class="form-group">
                        {{ Form::label('Location', trans('Location').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('location', 'location', null, ['class' => 'form-control', 'placeholder' => trans('Location')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('role', trans('I am a ').'*', ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            @foreach($roles as $role)

                            <!-- Group of default radios - option 1 -->
                            <div class="custom-control custom-radio">
                              {{ Form::radio('role[]', $role->id, true, ['class' => 'custom-control-input roles-selection','id'=>'defaultGroupExample'.$role->id,'data-id'=>$role->name]) }}
                              <label class="custom-control-label" for="defaultGroupExample{{$role->id}}">{{$role->name}}</label>
                            </div>
                            @endforeach
                        </div><!--col-md-6-->
                    </div><!--form-group-->


                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {{ Form::submit(trans('labels.frontend.auth.register_button'), ['class' => 'btn btn-primary']) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    {{ Form::close() }}

                </div><!-- panel body -->

            </div><!-- panel -->

        </div><!-- col-md-8 -->

    </div><!-- row -->
@endsection

@section('after-scripts')
    <script type="text/javascript">

        $(document).ready(function() {
            // To Use Select2
            Backend.Select2.init();

            $(document).on('change','.roles-selection',function(){
                    if($(this).data('id') == 'Doctor')
                    {
                        $('.Patient-div').css('display','none');
                        $('.Patient-div').find('input').attr('disabled','disabled');
                        $('.Patient-div').find('select').attr('disabled','disabled');
                        $('.Doctor-div').css('display','block');
                        $('.Doctor-div').find('input').removeAttr('disabled');
                    } else {
                        $('.Doctor-div').css('display','none');
                        $('.Doctor-div').find('input').attr('disabled','disabled');
                        $('.Patient-div').css('display','block');
                        $('.Patient-div').find('input').removeAttr('disabled');
                        $('.Patient-div').find('select').removeAttr('disabled');
                    }
            });


        });
    </script>
@endsection
