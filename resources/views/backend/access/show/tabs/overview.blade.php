<table class="table table-striped table-hover">
    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.name') }}</th>
        <td>{{ $user->first_name .' '. $user->last_name }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.email') }}</th>
        <td>{{ $user->email }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.status') }}</th>
        <td>{!! $user->status_label !!}</td>
    </tr>

    <tr>
        <th>{{ trans('Address') }}</th>
        <td>{!! $user->address !!}</td>
    </tr>

    @if($user->hasRole('Doctor'))
        <tr>
            <th>{{ trans('Skills') }}</th>
            <td>{!! $user->doctor_skill !!}</td>
        </tr>
    @endif

    @if($user->hasRole('Patient'))
        <tr>
            <th>{{ trans('Profession') }}</th>
            <td>{!! $user->patient_profession !!}</td>
        </tr>
    @endif


    <tr>
        <th>{{ trans('Location') }}</th>
        <td>{!! $user->location !!}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.created_at') }}</th>
        <td>{{ $user->created_at }} ({{ $user->created_at->diffForHumans() }})</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.last_updated') }}</th>
        <td>{{ $user->updated_at }} ({{ $user->updated_at->diffForHumans() }})</td>
    </tr>
</table>